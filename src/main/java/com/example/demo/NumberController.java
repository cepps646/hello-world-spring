package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NumberController {

    int result;

@GetMapping("/number")
public String computeNumbers(@RequestParam int seriesOfNumbers1, @RequestParam int seriesOfNumbers2, @RequestParam String operator){

    if (operator.equals("+") ) {

            result += seriesOfNumbers1;
            result += seriesOfNumbers2;
    }

    else if (operator.equals("*") ) {
        result = seriesOfNumbers1 * seriesOfNumbers2;
    }

    else if (operator.equals("-")) {
        result = seriesOfNumbers1 - seriesOfNumbers2;

    }

    else if (operator.equals("/")) {
        result = seriesOfNumbers1 - seriesOfNumbers2;

    }

    return String.valueOf(result);

}

}
