package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VowelCounter {

    int counter = 0;

    @GetMapping("/vowel")
    public String sumVowelsInString(@RequestParam String stringParam) {

        String lowerCaseStringParam = stringParam.toLowerCase();

        for (int i=0 ; i<lowerCaseStringParam.length(); i++){
            char ch = lowerCaseStringParam.charAt(i);
            if(ch == 'a'|| ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
            counter++;
            }
        }

        return String.valueOf(counter);

    }








}
