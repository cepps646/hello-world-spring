package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class NumberControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void numberComputationsAdditionTest() throws Exception {
        mockMvc.perform(get("/number?seriesOfNumbers1=1&seriesOfNumbers2=2&operator=+"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }


    @Test
    void numberComputationMultiplicationTest() throws Exception {
        mockMvc.perform(get("/number?seriesOfNumbers1=2&seriesOfNumbers2=3&operator=*"))
                .andExpect(status().isOk())
                .andExpect(content().string("6"));
    }

    @Test
    void numberComputationSubtractionTest() throws Exception {
        mockMvc.perform(get("/number?seriesOfNumbers1=4&seriesOfNumbers2=3&operator=-"))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));
    }


    @Test
    void numberComputationDivisionTest() throws Exception {
        mockMvc.perform(get("/number?seriesOfNumbers1=4&seriesOfNumbers2=2&operator=/"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));
    }


}
