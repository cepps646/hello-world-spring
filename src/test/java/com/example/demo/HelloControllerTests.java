package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;



    @Test
    void sayHello_noArgs_returnsHelloWorld() throws Exception {
        mockMvc.perform(get("/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));



    }

    @Test
   public void sayHello_myName_rtnHelloName() throws Exception {
        mockMvc.perform(get("/hello?name=Christian"))
                    .andExpect(status().isOk())
                .andExpect(content().string("Hello Christian"));
    }


}
