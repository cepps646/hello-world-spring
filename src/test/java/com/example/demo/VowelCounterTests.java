package com.example.demo;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class VowelCounterTests {

    @Autowired
    MockMvc mockMvc;


    @Test
    void vowelCountingA () throws Exception  {
        mockMvc.perform(get("/vowel?stringParam=AtlantaHawks"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("3"));

    }


    @Test
    void addingRemainingVowels () throws Exception  {
        mockMvc.perform(get("/vowel?stringParam=AtlantaHawksNation"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("6"));

    }

    @Test
    void toLowerCase() throws Exception  {
        mockMvc.perform(get("/vowel?stringParam=AtlantaHawksNation"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("7"));

    }


}
